# Description:
This script was made to validate password of users on Samba. It has some simple verifications, checks if have forbidden words and checks on Pwned database for leaked passwords.

# Use:
Just put the parameter "check password script" on smb.conf with the path to
this script and activate the "complexity check" on with "samba-tool".

# Configuration:
You can set the minimum number of chars for some checks and another things on
"configuration variables" section.

# Some words:
First, sorry for my bad english. :P

I love how technology can solve the most diverse problems. Acquiring knowledge, applying it, and sharing it is a great way to live. If possible, send a contribution. ~/o/

# Credits:
- Based on this topic: https://superuser.com/q/1168198
- Using API of Have I Been Pwned for leaked passwords: https://haveibeenpwned.com

# Changelog:
- 2018.07.04 - Initial Release.