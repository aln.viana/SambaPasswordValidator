#!/bin/bash

## ==============================================================================
## Written By: Allan Viana (aln.viana@outlook.com) - https://gitlab.com/aln.viana
## ==============================================================================
# Description:
# This script was made to validate password of users on Samba.
# It has some simple verifications, checks if have forbidden words and checks on
# Pwned database for leaked passwords.
#
# Use:
# Just put the parameter "check password script" on smb.conf with the path to
# this script and activate the "complexity check" on with "samba-tool".
#
# Configuration:
# You can set the minimum number of chars for some checks and another things on
# "configuration variables" section.
#
# Some words:
# First, sorry for my bad english. :P
# I love how technology can solve the most diverse problems.
# Acquiring knowledge, applying it, and sharing it is a great way to live.
# If possible, send a contribution. ~/o/
#
# Credits:
# Based on this topic: https://superuser.com/q/1168198
# Using API of Have I Been Pwned for leaked passwords: https://haveibeenpwned.com
#
# Changelog:
# 2018.07.04 - Initial Release.
## ==============================================================================

# Samba send the password to the program's standard input.
read pass

# [Configuration variables] =====================================================
min_length=12						# Minimal password length
min_alphachar=3						# Minimal letter characters
min_lower=1							# Minimal lowercase characters
min_upper=1							# Minimal uppercase characters
min_digit=3							# Minimal numeric characters
min_special=0						# Minimal special characters
specialchars="\!@#$%¨&*()_+=-§{}><~^"	# Special characters. This is a Regex String.

debug=false							# Activate to see some informations.
									# NOTE: You only will see something if run
									# this script manually.
# ===============================================================================

# [Controller variables] ========================================================
SCRIPT=$(realpath $0)
SCRIPTPATH=$(dirname $SCRIPT)

# Get password Hash to check on PWNED Database.
pass_sha1=$(echo -n "$pass" | sha1sum | awk '{print toupper($1)}')

ctr_length=0		# Password characters count
ctr_alphachar=0		# Uppercase characters count
ctr_upper=0			# Uppercase characters count
ctr_lower=0			# Lowercase characters count
ctr_digit=0			# Digit characters count
ctr_special=0		# Special characters count
# ===============================================================================

# [Process] =====================================================================

# Populate all control variables
ctr_length=$(printf "%s" "$pass" | wc -m)
for (( c=0; c<$ctr_length; c++ )); do
	tmp=${pass:$c:1}

	if [[ "$tmp" =~ [a-zA-Z] ]]; then
		ctr_alphachar=$[ctr_alphachar + 1]
	fi

	if [[ "$tmp" =~ [a-z] ]]; then
		ctr_lower=$[ctr_lower + 1]

	elif [[ "$tmp" =~ [A-Z] ]]; then
		ctr_upper=$[ctr_upper +1]

	elif [[ "$tmp" =~ [0-9] ]]; then
		ctr_digit=$[ctr_digit +1]

	elif [[ "$tmp" == *[$specialchars]* ]]; then
		ctr_special=$[ctr_special +1]
	fi
done

if [[ $debug == true ]]; then
	echo "Password: $pass"
	echo "Password SHA1: $pass_sha1"
	echo "Password SHA1 Head: ${pass_sha1:0:5}"
	echo "Password SHA1 Tail: ${pass_sha1:5:50}"
	echo ""
	echo "Length: $ctr_length - Min: $min_length"
	echo "Alpha: $ctr_alphachar - Min: $min_alphachar"
	echo "Lower: $ctr_lower - Min: $min_lower"
	echo "Upper: $ctr_upper - Min: $min_upper"
	echo "Digit: $ctr_digit - Min: $min_digit"
	echo "Special: $ctr_special - Min: $min_special"
	echo ""
fi

# Check if password is greater than necessary
if [[ $ctr_length -lt $min_length ]]; then
	echo "ERROR: Length less than allowed."
	exit 1
fi

# Check if password is greater or equals minimal alpha char count.
if [[ $ctr_alphachar -lt $min_alphachar ]]; then
	echo "ERROR: Alpha characters less than allowed."
	exit 1
fi

# Check if password is greater or equals minimal lower char count.
if [[ $ctr_lower -lt $min_lower ]]; then
	echo "ERROR: Lower characters less than allowed."
	exit 1
fi

# Check if password is greater or equals minimal upper char count.
if [[ $ctr_upper -lt $min_upper ]]; then
	echo "ERROR: Upper characters less than allowed."
	exit 1
fi

# Check if password is greater or equals minimal digit char count.
if [[ $ctr_digit -lt $min_digit ]]; then 
	echo "ERROR: Digit characters less than allowed."
	exit 2
fi

# Check if password is greater or equals minimal special char count.
if [[ $ctr_special -lt $min_special ]]; then
	echo "ERROR: Special characters less than allowed."
	exit 3
fi

# Check if password have triplicated characters.
if echo "$pass" | egrep -i '(.)\1{2}' > /dev/null; then
	echo "ERROR: Not allowed triplicated characters."
	exit 4
fi

# Check if password have forbidden words.
while IFS='' read -r line; do
	line=$(echo "$line" | tr -d '\r')
	if echo "$pass" | grep -i "$line" > /dev/null; then
		echo "ERROR: Forbidden word '$line'"
		exit 5
	fi
done < "$SCRIPTPATH/forbidden_words.txt"

# Check if password has already been leaked.
pwned_hashlist=$(wget -qO- https://api.pwnedpasswords.com/range/${pass_sha1:0:5})
if [[ $pwned_hashlist =~ .*${pass_sha1:5:50}.* ]]; then
	echo "ERROR: Password hash has found on PWNED Database."
	exit 6
fi

# In the end, if we reach this point, everything is right. ;D
exit 0